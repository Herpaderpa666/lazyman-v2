
package GameObj;


public interface IGameStream {
    
    public void setAwayMediaID(String id);
    
    public String getAwayMediaID();
    
    public void setHomeMediaID(String id);
    
    public String getHomeMediaID();
    
    public void setNationalMediaID(String id);
    
    public String getNationalMediaID();
    
    public void setFrenchMediaID(String id);
    
    public String getFrenchMediaID();
    
    public void setAwayTVStation(String station);
    
    public String getAwayTVStation();
    
    public void setHomeTVStation(String station);
    
    public String getHomeTVStation();
    
    public void setNationalTVStation(String station);
    
    public String getNationalTVStation();
    
    public void setFrenchTVStation(String station);
    
    public String getFrenchTVStation();
}
